SNMP Role
=========

This role performs actions related to SNMP.

The following tags are defined and used to control the actions of the role:

- report:       Creates a markdown file, per host, cataloguing SNMPv2 into a table. Files are created in the role/files directory.
- configure:    Configures SNMPv2 community strings, permissions (RO or RW), and associated access-list. Communities are referenced from the "snmp_communities" (list) variable.
- enforce:      Enforces the SNMP communities and associated settings defined in the "snmp_communities" (list) variable.

Requirements
------------

Ansible Galaxy Collections:
- cisco.ios

Python libraries:
- jinja2 (>3.1.0)

Role Variables
--------------

No variables are set in the role itself.

One variable is required to set SNMPv2 community strings and associated settings:

- Variable required:      snmp_communities
- Variable type:          list of dictionaries
- Variable structure:

```
[
        {
                name: (str)
                permission: ["RO"|"RW"] (str)
                acl_v4: (str)
        },
        {
                name: (str)
                permission: ["RO"|"RW"] (str)
                acl_v4: (str)
        }
]
```

```yaml
snmp_communities:
      - name: (str)
        permission: ["RO"|"RW"] (str)
        acl_v4: (str)
      - name: (str)
        permission: ["RO"|"RW"] (str)
        acl_v4: (str)
```
Each community dictionary must contain the following key/value pairs:

- name = SNMP community string
- permission = Permission, either read (RO) or write (RW)
- acl_v4 = Access-list (name or number), include a blank string if no ACL is required (e.g.: "")

N.B. Any referenced ACLs must exist prior to using the role.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

The following example calls the snmp role:

    - hosts: routers
      roles:
         - snmp

If no tags are passed, only the "report" function of the role will be run.
To run the "configure" or "enforce" functions, the appropriate tag must be passed when running the play, as well as the "snmp_communities" variable.

The following example passes the "snmp_communities" variable to the calling of the snmp role within the playbook, allowing the configuration of the defined SNMP community when the "configure" or "enforce" tags are used:

    - hosts: routers
      roles:
         - role: snmp
           vars:
             snmp_communities:
               - name: public
                 permission: RO
                 acl_v4: "readonly-acl"

License
-------

BSD

Author Information
------------------

Author: Anthony Gittins

Company: Computacenter

Date: 05/01/2023
